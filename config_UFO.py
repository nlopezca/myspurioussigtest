# -*- coding: utf-8 -*-

""" Common configuration for transfer factor (TF) background estimation class.

@file:   config.py
@author: Andreas Søgaard
@date:   1 May 2017
@email:  andreas.sogaard@cern.ch
"""

import numpy as np

config = {

    # Base path for input files
    #'base_path' : '/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijetISR/ntuples/FullRun2_Boosted/dijet/',
    #'base_path' : '/afs/cern.ch/work/j/jzahredd/dijetPlusISR/boostedBkgEstimate/Samples/',
    'base_path' : '/eos/user/n/nlopezca/v3oldtoysiginj_transferfactor/Samples/',
    #'base_path' : '/eos/user/j/jzahredd/dijetISR/',

    'sig_dir' : 'mc16a_jetJ_sig/',

    #'bkg_dir' : 'mc16a_jetJ_bkg/',
    'bkg_dir' : 'Flattener/',

    # Path to cross-sections file
    'xsec_file' : '/afs/cern.ch/work/j/jroloff/dijetPlusISR/boostedBkgEstimate/transferFactorEstimate/sampleInfo.csv',

    # Name of tree from which to read input data
    'tree'      : 'outTree',

    # Name of tree from which to read input data _afte_ tau21DDT cut
    'finaltree' : 'outTree',

    # Common prefix for branches in main tree; to be removed
    #'prefix'    : 'fatjet_',
    'prefix'    : 'UFOjet_',

    # Name of tree from which to read DSID and weight information
    'outputtree': 'outTree',

    # Luminosity
    'lumi'      : 36.1e3,

    # Parametrisation
    #'treeparams'    : ['tau21_wta', 'pt', 'ANN_score', 'E', 'eta', 'phi'],
    #'treeparams'    : ['tau21_wta', 'pt', 'ANN_score', 'm', 'eta', 'phi'],
    #'treeparams'    : ['tau21_wta[0]', 'pt[0]', 'ANN_score[0]', 'E[0]', 'eta[0]', 'phi[0]'],
    #'treeparams'    : ['tau21_wta[0]', 'pt[0]', 'ANN_score[0]', 'm[0]', 'eta[0]', 'phi[0]'],
    #'treeparams'    : ['tau21_wta[0]', 'pt[0]', 'ANN_score[0]', 'm[0]', 'eta[0]', 'phi[0]'],
    #'treeparams'    : ['tau21_wta', 'pt', 'ANN_score', 'm', 'eta', 'phi'],
    #'treeparams'    : ['tau21_wta', 'pt', 'ANN_score', 'm', 'eta', 'phi', 'weight'],
    #'treeparams'    : ['tau21_wta', 'pt', 'ANN_score', 'm', 'eta', 'phi', 'weight', 'sumweights'],
    'treeparams'    : ['tau21_wta', 'pt', 'ANN_score', 'm', 'eta', 'phi', 'weight', 'sumweights', 'DSID'],

    'params'    : ('rhoDDT', 'logpt'),
    'params_JMS'    : ('rhoDDT', 'pt'),
    'params_JMS_m'    : ('m', 'rhoDDT'),
    'params_JMS_logpt'    : ('m', 'logpt'),
    'params_JMS_pt'    : ('m', 'pt'),

#    'tagger'    : 'tau21DDT',
    'tagger'    : 'ANN_score',

    #'ptCut'      : lambda data: (data['pt'] > 400)*(data['pt']<1200)*(data['rhoDDT']>1.5)*(data['rhoDDT']<5.0) * ( data['pt']/data['m'] > 2 ),
    #'ptCut'      : lambda data: (data['pt'] > 450)*(data['rhoDDT']>1.5) * ( data['pt']/data['m'] > 2 ), ## CONF note-like cuts

    #'pass'      : lambda data: (data['tau21DDT'] < 0.5) * (data['tau21DDT'] > 0.),
    #'pass'      : lambda data: data['tau21DDT'] < 0.5,
    #'pass'      : lambda data: data['tau21DDT'] < -0.07,
    'pass'      : lambda data: data['ANN_score'] > 0.55,
    #'pass'      : lambda data: data['ANN_score'] > 0.00,

    # Axis definitions
    'axes'      : [
        #np.linspace(1.5,         5.0,          70 + 1, endpoint=True), # x-axis: rhoDDT
        #np.linspace(1.5,         5.,          90 + 1, endpoint=True), # x-axis: rhoDDT
        #np.linspace(np.log(200), np.log(1000), 10 + 1, endpoint=True), # y-axis: log(pT)
        #np.linspace(np.log(200), np.log(1200), 10 + 1, endpoint=True), # y-axis: log(pT)
        #np.linspace(np.log(400), np.log(2000), 30 + 1, endpoint=True), # y-axis: log(pT)
        #np.linspace(np.log(200), np.log(1200), 10 + 1, endpoint=True), # y-axis: log(pT)
        #np.linspace(np.log(400), np.log(2000), 30 + 1, endpoint=True), # y-axis: log(pT)
        #np.linspace(1.5,         5.0,          90 + 1, endpoint=True), # x-axis: rhoDDT
        #np.linspace(1.5,         5.0,          90 + 1, endpoint=True), # x-axis: rhoDDT
        #np.linspace(-5.2,         -1.3,          100 + 1, endpoint=True), # x-axis: rhoDDT
        #np.linspace(-6.2,         -1.3,          90 + 1, endpoint=True), # x-axis: rhoDDT 
        np.linspace(-5.2,         -1.3,          100 + 1, endpoint=True), # x-axis: rhoDDT 
        #np.linspace(np.log(400), np.log(1000), 30 + 1, endpoint=True), # y-axis: log(pT)
        np.linspace(np.log(400), np.log(1000), 40 + 1, endpoint=True), # y-axis: log(pT)
        #np.linspace(np.log(450), np.log(1000), 9 + 1, endpoint=True), # y-axis: log(pT)
        #np.array([5.99146454711,  6.0830936203,  6.26635176667,  6.54123898623,  6.90775527898],dtype=float),
        #np.linspace(np.log(400), np.log(1000), 9 + 1, endpoint=True), # y-axis: log(pT)
        ],

    'axes_fine' : [
        #np.linspace(1.5,         5.0,          10 * 70 + 1, endpoint=True), # x-axis: rhoDDT
        #np.linspace(1.5,         5.,          10 * 90 + 1, endpoint=True), # x-axis: rhoDDT
        #np.linspace(np.log(200), np.log(1000), 10 * 10 + 1, endpoint=True), # y-axis: log(pT)
        #np.linspace(np.log(200), np.log(1200), 10 * 10 + 1, endpoint=True), # y-axis: log(pT)
        #np.linspace(np.log(400), np.log(2000), 10 * 30 + 1, endpoint=True), # y-axis: log(pT)
        #np.linspace(1.5,         6.0,          10 * 90 + 1, endpoint=True), # x-axis: rhoDDT
        #np.linspace(1.5,         5.,          10 * 90 + 1, endpoint=True), # x-axis: rhoDDT
        #np.linspace(np.log(450), np.log(1000), 10 * 9 + 1, endpoint=True), # y-axis: log(pT)
        #np.linspace(np.log(200), np.log(1200), 10 * 10 + 1, endpoint=True), # y-axis: log(pT)
        #np.linspace(np.log(400), np.log(2000), 10 * 30 + 1, endpoint=True), # y-axis: log(pT)
        #np.linspace(1.5,         5.0,          10 * 90 + 1, endpoint=True), # x-axis: rhoDDT 
        #np.linspace(1.5,         5.0,          10 * 90 + 1, endpoint=True), # x-axis: rhoDDT
        #np.linspace(-5.2,         -1.3,          10 * 100 + 1, endpoint=True), # x-axis: rhoDDT 
        #np.linspace(-6.2,         -1.3,          10 * 90 + 1, endpoint=True), # x-axis: rhoDDT
        np.linspace(-5.2,         -1.3,          10 * 100 + 1, endpoint=True), # x-axis: rhoDDT
        #np.linspace(np.log(400), np.log(1000), 10 * 30 + 1, endpoint=True), # y-axis: log(pT)
        np.linspace(np.log(400), np.log(1000), 10 * 40 + 1, endpoint=True), # y-axis: log(pT)
        #np.linspace(np.log(450), np.log(1000), 10 * 9 + 1, endpoint=True), # y-axis: log(pT) 
        #np.array([6.90775527898,  6.90775527898,  6.90775527898,  6.90775527898,  6.90775527898,  6.90775527898,  6.90775527898,  6.90775527898,  6.90775527898,  6.90775527898,  6.90775527898,  6.90775527898,  6.90775527898,  6.90775527898,  6.90775527898,  6.90775527898,  6.90775527898,  6.90775527898,  6.90775527898,  6.90775527898,  6.90775527898,  6.90775527898,  6.90775527898,  6.90775527898,  6.90775527898,  6.90775527898,  6.90775527898,  6.90775527898,  6.90775527898,  6.90775527898,  6.90775527898,  6.90775527898,  6.90775527898,  6.90775527898,  6.90775527898,  6.90775527898,  6.90775527898,  6.90775527898,  6.90775527898,  6.90775527898,  6.90775527898],dtype=float),
        #np.linspace(np.log(400), np.log(1000), 10 * 9 + 1, endpoint=True), # y-axis: log(pT)
        ],


    # Jet mas spectrum bin edges
    'massbins' : np.linspace(50, 300, 50 + 1, endpoint=True),
    #'massbins' : np.linspace(20, 300, 50 + 1, endpoint=True),
    #'massbins': np.array([50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100, 105, 110, 115, 120, 126, 132, 138, 145, 152, 159, 167, 175, 183, 191, 200, 210, 220, 232, 245, 259, 274, 300], dtype=float),

    }


# Bin centres
config['centres'] = [
    config['axes'][0][:-1] + 0.5 * np.diff(config['axes'][0]),
    config['axes'][1][:-1] + 0.5 * np.diff(config['axes'][1])
]

config['centres_fine'] = [
    config['axes_fine'][0][:-1] + 0.5 * np.diff(config['axes_fine'][0]),
    config['axes_fine'][1][:-1] + 0.5 * np.diff(config['axes_fine'][1])
]
