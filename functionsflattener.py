# -*- coding: utf-8 -*-
""" Collection of python utility functions.
...
"""

# Basic include(s)
import sys
import math
import itertools
import sys, glob

from ROOT  import *
from array import *
from collections import namedtuple

# Scientific include(s); require correct python environment
try:
    import numpy as np
    from numpy.lib.recfunctions import append_fields
    #from rootplotting.tools import *
    from root_numpy import tree2array
except:
    print "ERROR: Scientific python packages were not set up properly."
    print " $ source snippets/pythonenv.sh"
    print "or see e.g. [http://rootpy.github.io/root_numpy/start.html]."
    raise

# Global variables
colours = [kViolet + 7, kAzure + 7, kTeal, kSpring - 2, kOrange - 3, kPink]

def getDataDictJMS(cfile, branches, config, jetcol, doNorm = True):


    values = dict()
    data = {}
    mass = []
    pt = []
    eta = []
    phi = []
    ANN_score = []
    tau21_wta = []
    weight = []
    weightsum = []
    dsid = []

    paths = [cfile]
    for ipath, path in enumerate(paths):
        print ipath, path
        # Print progress.
        print "\rloadData:   [%-*s]" % (len(paths), '-' * (ipath + 1)),

        # Get file.
        f = TFile(path, 'READ')

        # Get tree.
        if jetcol=="UFOjet_": treename = "UFO"
        if jetcol=="fatjet_": treename = "LCTopo"

        tree = f.Get(treename)
        if not tree:
            print "\rloadData:   Tree '%s' was not found in file '%s'. Skipping." % (jetcol.replace("_",""), path)
            f.Close()
            continue

        if jetcol=="UFOjet_": prefixvar = "UFO_"
        if jetcol=="fatjet_": prefixvar = "fatjet_"

        arr = tree2array(tree, branches = [prefixvar + var for var in branches], include_weight = False)


        for var in arr.dtype.names:
            if "_tau21_wta" in var  : tau21_wta.extend(arr[var].tolist())
            if "_pt" in var         : pt.extend(arr[var].tolist())
            if "_ANN_score" in var  : ANN_score.extend(arr[var].tolist())
            if "_m" in var          : mass.extend(arr[var].tolist())
            if "_eta" in var        : eta.extend(arr[var].tolist())
            if "_phi" in var        : phi.extend(arr[var].tolist())
            if "_weight" in var     : weight.extend(arr[var].tolist())
            if "_sumweights" in var : weightsum.extend(arr[var].tolist())
            if "_DSID" in var       : dsid.extend(arr[var].tolist())
            pass



    data['m'] =  mass
    data['pt'] = pt
    data['eta'] = eta
    data['phi'] = phi
    data['ANN_score'] = ANN_score
    data['tau21_wta'] = tau21_wta
    data['weight'] = weight
    data['sumweights'] = weightsum
    data['DSID'] = [int(x) for x in dsid]

    data['logpt'] = np.log(pt)

    data['rhoDDT'] = np.log(np.maximum(np.divide(np.multiply(data['m'], data['m']),np.multiply(data['pt'], data['pt'])), np.ones_like(data['pt'])*1e-6))
#    data['rhoDDT'] = np.log(np.maximum(np.divide( np.multiply(data['m'], data['m']), np.maximum(data['pt'], np.ones_like(data['pt'])*1e-6)), np.ones_like(data['pt'])*1e-6) )

    if jetcol=="fatjet_": data['tau21DDT'] = data['tau21_wta'] - data['rhoDDT']*(-0.094)
    if jetcol=="UFOjet_":
        pa0=0.464199
        pa1=0.0313991
        pa2=-0.121008
        pa3=0.0836385
        pa4=-0.025045
        pa5=0.00261024
        rhoprime=(data['rhoDDT']-1.5)
        data['tau21DDT'] = data['tau21_wta'] -(pa5)*(rhoprime**5)-(-0.025045)*(rhoprime**4)-(0.0836385)*(rhoprime**3)-(-0.121008)*(rhoprime**2)-(0.0313991)*(rhoprime)-0.464199
    data['tau21DDT'] = np.nan_to_num(data['tau21DDT'])
    data['rhoDDT'] = np.nan_to_num(data['rhoDDT'])


    if doNorm:
        data['weight'] = np.asarray(data['weight']).flatten() * config['lumi'] / np.asarray(data['sumweights']).flatten() # Scale all events (MC) by luminosity
    else:
        data['weight'] = np.asarray(data['weight']) / 1. # not sure what's the use of this now

    return data



def dict_product(dicts):
    """ Returns cartesian product of dict entries, as a dict with one-entry value arrays, using itertools.product """
    return (dict(itertools.izip(dicts, x)) for x in itertools.product(*dicts.itervalues()))



def getSignalFiles(base_path, signal_dir, mass):
    newMass = mass
    while newMass % 10  == 0:
      newMass = mass/10

    print newMass
    print base_path + signal_dir
    sigfiles  = glob.glob(base_path + signal_dir + 'user.cdelitzs.*mRp%d*.root/*.tree.root'%(newMass))
    print sigfiles
    return sigfiles
