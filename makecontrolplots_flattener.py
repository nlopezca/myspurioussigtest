#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Script for studying the closure of the transfer factor method on MC.

@file:   closure.py
@author: Andreas Søgaard
@date:   4 April 2017
@email:  andreas.sogaard@cern.ch
"""

# Basic import(s)
import sys, glob

# Get ROOT to stop hogging the command-line options
import ROOT
import ROOT as r
ROOT.PyConfig.IgnoreCommandLineOptions = True
from calculator_JMS import calculator
import functionsflattener
#from config import config
import pandas as pd
import DrawingFunctions as df
from variables import variables
# Scientific import(s)
try:
    # Numpy
    import numpy as np
    from root_numpy import *
except ImportError:
    print "WARNING: One or more scientific python packages were not found. If you're in lxplus, try running:"
    print "         $ source /cvmfs/sft.cern.ch/lcg/views/LCG_88/x86_64-slc6-gcc49-opt/setup.sh"
    sys.exit()
    pass

# Local import(s)
try:
    #import transferfactor as tf
    from rootplotting import ap
    from rootplotting.tools import *
except ImportError:
    print "WARNING: This script uses the 'transferfactor' and 'rootplotting' packages. Clone them as e.g.:"
    print "         $ git clone git@github.com:asogaard/transferfactor.git"
    print "         $ git clone git@github.com:asogaard/rootplotting.git"
    sys.exit()
    pass


# Command-line arguments parser
import argparse

parser = argparse.ArgumentParser(description='Perform closure test.')

parser.add_argument('--rhoDDTminCut', dest='rhoDDTminCut', type=float,
                    default=1.5,
                    help='Lower cut on rhoDDT (default: 1.5)')
parser.add_argument('--rhoDDTmaxCut', dest='rhoDDTmaxCut', type=float,
                    default=999,
                    help='Higher cut on rhoDDT (default: 999)')
parser.add_argument('--ptCut', dest='ptCut', type=float,
                    default=450,
                    help='Cut on fatjet pT (default: 450)')
parser.add_argument('--jetcol', dest='jetcol', type=str,
                    default='',
                    help='fatjet_ or UFOjet_')
parser.add_argument('--outdir', dest='outdir', type=str,
                    default='',
                    help='Output directory')
parser.add_argument('--save', dest='save', action='store_const',
                    const=True, default=False,
                    help='Save plots (default: False)')

args = parser.parse_args()

if args.jetcol=="fatjet_":
    from config import config
if args.jetcol=="UFOjet_":
    from config_UFO import config

# Main function.
def main ():

    # Parse command-line arguments

    # Setup.
    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    # Load data
    #files  = glob.glob(tf.config['base_path'] + 'objdef_MC_3610*.root')
    #files  = glob.glob(config['base_path'] + config['bkg_dir'] + 'JZ12/*01.tree.root*')
    #files  = glob.glob(config['base_path'] + config['bkg_dir'] + '/QCD_MC16a_JZ3.root')
    #files  = glob.glob(config['base_path'] + config['bkg_dir'] + '/Sig_MC16a_mass175.root')
    files  = glob.glob(config['base_path'] + config['bkg_dir'] + '/QCD_MC16a_JZ7.root')
    #files  = glob.glob(config['base_path'] + config['bkg_dir'] + '/QCD_MC16a_JZ11.root')
    #files  = glob.glob(config['base_path'] + config['bkg_dir'] + 'group.phys-exotics.DiJetISR.mc16a.*.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ*WithSW.ntuples_JETJ_20220216_v1/*.tree.root*')

    if len(files) == 0:
        warning("No files found.")
        return


    ControlPlots_loadData(files, config['treeparams'], save=args.save, ptCut=args.ptCut, rhoDDTminCut=args.rhoDDTminCut,  rhoDDTmaxCut=args.rhoDDTmaxCut,
                        jetcol=args.jetcol, prefix=args.outdir)


def ControlPlots_loadData (files, branches, save, ptCut, rhoDDTminCut, rhoDDTmaxCut, jetcol, prefix = '', fast = True):

    try:
        os.system("mkdir {}".format(prefix))
    except ImportError:
        print("{} already exists".format(prefix))
        pass

    bins = config['massbins']
    count = 0
    key_to_del = ['ANN_score', 'tau21_wta']
    for cfile in files:
        data1 = functionsflattener.getDataDictJMS(cfile, branches, config, jetcol, doNorm=True)
        if not data1:
            continue


        if (count == 0):
            for key in key_to_del:
                del data1[key]
            dataframe = pd.DataFrame.from_dict(data1)
            dataframe = dataframe[(dataframe['pt'] > ptCut) & (dataframe['rhoDDT'] > rhoDDTminCut) & (dataframe['rhoDDT'] < rhoDDTmaxCut) &  (dataframe['pt'] / dataframe['m'] > 2)]
            #dataframe = dataframe[(dataframe['pt'] > ptCut) & (dataframe['rhoDDT'] > rhoDDTminCut) & (dataframe['rhoDDT'] < rhoDDTmaxCut)]
            #dataframe = dataframe[(dataframe['pt'] > ptCut)]
            print(len(dataframe.index))
        else:
            #print('count is: {}'.format(count))
            for key in key_to_del:
                del data1[key]
            df_temp = pd.DataFrame.from_dict(data1)
            df_temp = df_temp[(df_temp['pt'] > ptCut) & (df_temp['rhoDDT'] > rhoDDTminCut) & (df_temp['rhoDDT'] < rhoDDTmaxCut) &  (df_temp['pt'] / df_temp['m'] > 2)]
            #df_temp = df_temp[(df_temp['pt'] > ptCut) & (df_temp['rhoDDT'] > rhoDDTminCut) & (df_temp['rhoDDT'] < rhoDDTmaxCut)]
            #df_temp = df_temp[(df_temp['pt'] > ptCut) &  (df_temp['pt'] / df_temp['m'] > 2)]
            print(len(df_temp.index))
            dataframe = dataframe.append(df_temp, ignore_index = True)

        count +=1

    print(dataframe.head())
    columns = np.array(dataframe.columns.values.tolist())

    for column in columns:
        if column in variables.keys():
            print(column)
            if column=='weight':
                continue
            varbins = variables[column]["Bins"].split(",")
            h_varDistribution  = r.TH1F("h_var%s"%column, "; %s;a.u."%(variables[column]["PrettyName"]), int(varbins[0]), float(varbins[1]), float(varbins[2]))
            bins = np.linspace(float(varbins[1]), float(varbins[2]), int(varbins[0]) + 1, endpoint=True)
            c1 = ap.canvas(num_pads=1, batch=True)
            p0= c1.pads()
            if column=='pt': h_varDistribution = c1.plot(np.array(dataframe.pt), bins=bins, weights=np.array(dataframe.weight))
            if column=='eta': h_varDistribution = c1.plot(np.array(dataframe.eta), bins=bins, weights=np.array(dataframe.weight))
            if column=='phi': h_varDistribution = c1.plot(np.array(dataframe.phi), bins=bins, weights=np.array(dataframe.weight))
            if column=='m': h_varDistribution = c1.plot(np.array(dataframe.m), bins=bins, weights=np.array(dataframe.weight))
            if column=='rhoDDT': h_varDistribution = c1.plot(np.array(dataframe.rhoDDT), bins=bins, weights=np.array(dataframe.weight))
            if column=='tau21DDT': h_varDistribution = c1.plot(np.array(dataframe.tau21DDT), bins=bins, weights=np.array(dataframe.weight))
            if column=='logpt': h_varDistribution = c1.plot(np.array(dataframe.logpt), bins=bins, weights=np.array(dataframe.weight))

            c1.xlabel('%s'%(variables[column]["PrettyName"]))
            c1.ylabel('Events')

            c1.text(["#sqrt{s} = 13 TeV,  L = 36.1 fb^{-1}",
                    #"Sherpa incl. #gamma MC",
                    #"Trimmed anti-k_{t}^{R=1.0} jets",
                    #"ISR #gamma selection",
                    ], qualifier='Simulation Internal')
            #c1.log()
            if save: c1.save(prefix+"/distribution_{}{}.png".format(jetcol,column))

    return


# Main function call.
if __name__ == '__main__':
    main()
    pass
