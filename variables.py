variables = {}

variables["tau21DDT"] = { "PrettyName": "#tau_{21}^{DDT}",
                          "Units": "",
                          "Bins": "50,0,1",
                        }

variables["logpt"] = { "PrettyName": "log(Jet p_{T})",
                          "Units": "",
                          "Bins": "50,-10,2",
                      }

variables["pt"] = { "PrettyName": "Jet p_{T}",
                          "Units": "GeV",
                          "Bins": "200,200,2000",
                          #"Bins": "500,200,5000",
                      }


variables["m"] = { "PrettyName": "Jet mass",
                          "Units": "GeV",
                          "Bins": "50,50,300",
                      }


variables["rhoDDT"] = { "PrettyName": "#rho^{DDT}",
                          "Units": "",
                          "Bins": "50,0.01,10",
                      }

variables["eta"] = { "PrettyName": "Jet #eta",
                          "Units": "",
                          "Bins": "50,-3.,3.",
                      }

variables["phi"] = { "PrettyName": "Jet #phi",
                          "Units": "",
                          #"Bins": "50,-3.14,3.14",
                          "Bins": "50,-4,4",
                      }
variables["ANN_score"] = { "PrettyName": "ANN score",
                          "Units": "",
                          "Bins": "50,0,1",
                        }
