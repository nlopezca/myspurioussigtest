#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Script for performing signal injection tests.

@file:   signalinjection.py
@author: Andreas Søgaard
@date:   24 April 2017
@email:  andreas.sogaard@cern.ch
"""

# Basic import(s)
import sys, glob

# Get ROOT to stop hogging the command-line options
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
from calculator_JMS import calculator
import functionsflattener
import pandas as pd

# Scientific import(s)
try:
    # Numpy
    import numpy as np
    from root_numpy import *
except ImportError:
    print "WARNING: One or more scientific python packages were not found. If you're in lxplus, try running:"
    print "         $ source /cvmfs/sft.cern.ch/lcg/views/LCG_88/x86_64-slc6-gcc49-opt/setup.sh"
    sys.exit()
    pass

# Local import(s)
try:
    #import transferfactor as tf
    #from transferfactor.utils import get_signal_DSID, get_histogram
    from utils import get_signal_DSID, get_histogram
    from rootplotting import ap
    from rootplotting.tools import *
except ImportError:
    print "WARNING: This script uses the 'transferfactor' and 'rootplotting' packages. Clone them as e.g.:"
    print "         $ git clone git@github.com:asogaard/transferfactor.git"
    print "         $ git clone git@github.com:asogaard/rootplotting.git"
    sys.exit()
    pass


# Command-line arguments parser
import argparse

parser = argparse.ArgumentParser(description='Perform signal injection test.')

parser.add_argument('--mass', dest='mass', type=int,
                    required=True,
                    help='Center of excluded mass window')
parser.add_argument('--window', dest='window', type=float,
                    default=0.2,
                    help='Relative width of excluded mass window (default: 0.2)')
parser.add_argument('--rhoDDTminCut', dest='rhoDDTminCut', type=float,
                    default=1.5,
                    help='Lower cut on rhoDDT (default: 1.5)')
parser.add_argument('--rhoDDTmaxCut', dest='rhoDDTmaxCut', type=float,
                    default=999,
                    help='Higher cut on rhoDDT (default: 999)')
parser.add_argument('--ptCut', dest='ptCut', type=float,
                    default=450,
                    help='Cut on fatjet pT (default: 450)')
parser.add_argument('--show', dest='show', action='store_const',
                    const=True, default=False,
                    help='Show plots (default: False)')
parser.add_argument('--save', dest='save', action='store_const',
                    const=True, default=False,
                    help='Save plots (default: False)')
parser.add_argument('--inject', dest='inject', action='store_const',
                    const=True, default=False,
                    help='Inject signal (default: False)')
parser.add_argument('--toys', dest='toys', action='store_const',
                    const=True, default=False,
                    help='Use toys (default: False)')
parser.add_argument('--jetcol', dest='jetcol', type=str,
                    default='',
                    help='fatjet_ or UFOjet_')
parser.add_argument('--outdir', dest='outdir', type=str,
                    default='',
                    help='Output directory')
parser.add_argument('--SigStrength', dest='SigStrength', type=int,
                    default=999,
                    help='Signal strength')

args = parser.parse_args()

if args.jetcol=="fatjet_":
    from config import config
if args.jetcol=="UFOjet_":
    from config_UFO import config

#python signalInjection_JMS.py --mass 175 --window 0.2 --jetcol fatjet_ --rhoDDTminCut 1.5 --rhoDDTmaxCut 5 --ptCut 450 --outdir testJad --save --inject
#python signalInjection_JMS.py --mass 175 --window 0.2 --jetcol fatjet_ --rhoDDTminCut 1.5 --rhoDDTmaxCut 5 --ptCut 450 --outdir testJad --save --inject
#python signalInjection1.py --mass 175 --window 0.2 --jetcol fatjet_ --rhoDDTminCut 1.5 --rhoDDTmaxCut 5 --ptCut 450 --outdir testbkgJad --save --inject
#python signalInjection1.py --SigStrength 1 --mass 175 --window 0.2 --jetcol fatjet_ --rhoDDTminCut 1.5 --rhoDDTmaxCut 5 --ptCut 450 --outdir testbkgJad --save --inject
#python signalInjection1.py --SigStrength 10 --mass 175 --window 0.2 --jetcol fatjet_ --rhoDDTminCut 1.5 --rhoDDTmaxCut 5 --ptCut 450 --outdir testbkgJad --save --inject
#python signalInjection_wFail_simpleFit.py --SigStrength 10 --mass 175 --window 0.2 --jetcol fatjet_ --rhoDDTminCut 1.5 --rhoDDTmaxCut 5 --ptCut 450 --outdir skewedWindows --save --inject
#python signalInjection_wFail_simpleFit.py --SigStrength 10 --mass 100 --window 0.2 --jetcol fatjet_ --rhoDDTminCut 1.5 --rhoDDTmaxCut 5 --ptCut 450 --outdir skewedWindows --save --inject
#python signalInjection_wFail_simpleFit.py --SigStrength 10 --mass 125 --window 0.2 --jetcol fatjet_ --rhoDDTminCut 1.5 --rhoDDTmaxCut 5 --ptCut 450 --outdir skewedWindows --save --inject
#python signalInjection_wFail_simpleFit.py --SigStrength 10 --mass 150 --window 0.2 --jetcol fatjet_ --rhoDDTminCut 1.5 --rhoDDTmaxCut 5 --ptCut 450 --outdir skewedWindows --save --inject
# python signalInjection_wFail_simpleFit.py --SigStrength 10 --mass 175 --window 0.2 --jetcol fatjet_ --rhoDDTminCut 1.5 --rhoDDTmaxCut 5 --ptCut 450 --outdir skewedWindows --save --inject
# python signalInjection_wFail_simpleFit.py --SigStrength 10 --mass 200 --window 0.2 --jetcol fatjet_ --rhoDDTminCut 1.5 --rhoDDTmaxCut 5 --ptCut 450 --outdir skewedWindows --save --inject
# python signalInjection_wFail_simpleFit.py --SigStrength 10 --mass 225 --window 0.2 --jetcol fatjet_ --rhoDDTminCut 1.5 --rhoDDTmaxCut 5 --ptCut 450 --outdir skewedWindows --save --inject
# python signalInjection_wFail_simpleFit_toys.py --SigStrength 1 --mass 175 --window 0.2 --jetcol fatjet_ --rhoDDTminCut 1.5 --rhoDDTmaxCut 5 --ptCut 450 --outdir skewedWindows_toys --save --inject --toys

# Main function.
def main ():

    # Parse command-line arguments
    #args = parser.parse_args()
    ROOT.TH1.SetDefaultSumw2(True)

    # Setup.
    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    # Get signal file
    #sig_DSID = get_signal_DSID(args.mass, tolerance=10)
    #if sig_DSID is None:
    #    return
    #sig_file = 'objdef_MC_{DSID:6d}.root'.format(DSID=sig_DSID)
    #sig_file = 'mass{}/*_v1'.format(args.mass)
    #print(sig_file)

    # Load data
    files  = glob.glob(config['base_path'] + config['bkg_dir'] + '/QCD_MC16a_JZ6_test.root')
    files += glob.glob(config['base_path'] + config['bkg_dir'] + "/Sig_MC16a_mass{}_test.root".format(args.mass))


    print(files)
    if len(files) == 0:
        warning("No files found.")
        return

    signalinjection_loadData(files, config['treeparams'], mass=args.mass, window=args.window, save=args.save, SigStrength=args.SigStrength, ptCut=args.ptCut,
                        rhoDDTminCut=args.rhoDDTminCut,  rhoDDTmaxCut=args.rhoDDTmaxCut, jetcol=args.jetcol, prefix=args.outdir)


def signalinjection_loadData (files, branches, mass, window, save, SigStrength, ptCut, rhoDDTminCut, rhoDDTmaxCut, jetcol, prefix = '', fast = True):

    try:
        os.system("mkdir {}".format(prefix))
    except ImportError:
        print("{} already exists".format(prefix))
        pass

    bins = config['massbins']
    count = 0
    #key_to_del = ['ANN_score', 'ANN_score[0]', 'eta[0]', 'm[0]', 'phi[0]', 'pt[0]', 'px', 'py', 'pz', 'tau21_wta', 'tau21_wta[0]', 'weight_xs']
    key_to_del = [ 'tau21_wta']
    for cfile in files:
        data1 = functionsflattener.getDataDictJMS(cfile, branches, config, jetcol, doNorm=True)
        if not data1:
            continue


        if (count == 0):
            for key in key_to_del:
                del data1[key]
            dataframe = pd.DataFrame.from_dict(data1)
            #dataframe = dataframe[dataframe['pt'] < 2000]
            dataframe = dataframe[(dataframe['pt'] > ptCut) & (dataframe['rhoDDT'] > rhoDDTminCut) & (dataframe['rhoDDT'] < rhoDDTmaxCut) &  (dataframe['pt'] / dataframe['m'] > 2)]
            print(len(dataframe.index))
        else:
            #print('count is: {}'.format(count))
            for key in key_to_del:
                del data1[key]
            df_temp = pd.DataFrame.from_dict(data1)
            #df_temp = df_temp[df_temp['pt'] < 2000]
            df_temp = df_temp[(df_temp['pt'] > ptCut) & (df_temp['rhoDDT'] > rhoDDTminCut) & (df_temp['rhoDDT'] < rhoDDTmaxCut) &  (df_temp['pt'] / df_temp['m'] > 2)]
            print(len(df_temp.index))
            dataframe = dataframe.append(df_temp, ignore_index = True)

        count +=1


    sig_DSID = get_signal_DSID(mass, tolerance=10)
    if sig_DSID is None:
        return
    print(sig_DSID)
    #testNpass=int(np.sum(data['weight'][msk_pass]))
    #print("testNpass")
    #print(testNpass)
    data = dict()
    signal = dict()
    bkg = dict()

    # -- Separate signal and background
    dataframe_signal = dataframe[dataframe["DSID"] == sig_DSID]
    dataframe_bkg    = dataframe[dataframe["DSID"] != sig_DSID]

    # -- Tweak dataframes (this value should be mu)
    if args.inject:
        dataframe_signal1      = pd.concat([dataframe_signal]*SigStrength, ignore_index=True)
        dataframe_all         = pd.concat([dataframe_signal1, dataframe_bkg], ignore_index=True)
    else:
        dataframe_all = dataframe_bkg

    # -- Convert dataframes to dict
    data        = dataframe_all.to_dict('list')
    signal      = dataframe_signal.to_dict('list')
    bkg         = dataframe_bkg.to_dict('list')
    print("lengths")
    print(len(dataframe.index))
    print(len(dataframe_signal.index))
    print(len(dataframe_bkg.index))

    #testNpass=int(np.sum(data['weight'][msk_pass]))
    #print("testNpass")
    #print(testNpass)

    for key in data:
        data[key]        = np.array(data[key])
        signal[key]      = np.array(signal[key])
        bkg[key]         = np.array(bkg[key])


    # Toys -- do not worry about this now
    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    if args.toys:
        # Get masks
        msk_pass = config['pass'](data)
        msk_fail = ~msk_pass
        msk_sig  = (data['DSID'] == sig_DSID)
        msk_data = ~msk_sig

        # Create histograms
        if args.inject:
            pdf_pass = get_histogram(data, config['params'], config['axes'], mask=msk_pass & ~msk_sig)
            pdf_fail = get_histogram(data, config['params'], config['axes'], mask=msk_fail & ~msk_sig)
        else:
            pdf_pass = get_histogram(data, config['params'], config['axes'], mask=msk_pass)
            pdf_fail = get_histogram(data, config['params'], config['axes'], mask=msk_fail)
            pass

        # Smooth (only leading background)
        for _ in range(2):
            pdf_pass.Smooth()
            pdf_fail.Smooth()
            pass

        # Inject afterwards
        if args.inject:
            pdf_pass.Add(get_histogram(data, config['params'], config['axes'], mask=msk_pass & msk_sig))
            pdf_fail.Add(get_histogram(data, config['params'], config['axes'], mask=msk_fail & msk_sig))


        # Create p.d.f.s
        # -- Define variables
        rhoDDT = ROOT.RooRealVar('rhoDDT', 'rhoDDT', config['axes'][0][0], config['axes'][0][-1])
        logpt  = ROOT.RooRealVar('logpt',  'logpt',  config['axes'][1][0], config['axes'][1][-1])

        rhoDDT.setBins(len(config['axes'][0]) - 1)
        logpt .setBins(len(config['axes'][1]) - 1)

        # -- Define histograms
        rdh_pass = ROOT.RooDataHist('rdh_pass', 'rdh_pass', ROOT.RooArgList(rhoDDT, logpt), pdf_pass)
        rdh_fail = ROOT.RooDataHist('rdh_fail', 'rdh_fail', ROOT.RooArgList(rhoDDT, logpt), pdf_fail)

        # -- Turn histograms into pdf's
        rhp_pass = ROOT.RooHistPdf('rhp_pass', 'rhp_pass', ROOT.RooArgSet(rhoDDT, logpt), rdh_pass)
        rhp_fail = ROOT.RooHistPdf('rhp_fail', 'rhp_fail', ROOT.RooArgSet(rhoDDT, logpt), rdh_fail)

        # Generate toys
        #mult = 100.
        mult = 1.
        print("I am here and")
        print(data['weight'][msk_pass])
        N_pass = int(np.sum(data['weight'][msk_pass]) * mult)
        N_fail = int(np.sum(data['weight'][msk_fail]) * mult)
        print("(np.sum(data['weight'])")
        print(np.sum(data['weight']))
        print("(np.sum(data['weight'][msk_pass])")
        print(np.sum(data['weight'][msk_pass]))
        print("(np.sum(data['weight'][msk_fail])")
        print(np.sum(data['weight'][msk_fail]))
        print("size all")
        print((data['weight']).size)
        print(np.prod((data['weight']).shape))
        print("size pass")
        print((data['weight'][msk_pass]).size)
        print(np.prod((data['weight'][msk_pass]).shape))
        print("size fail")
        print((data['weight'][msk_fail]).size)
        print(np.prod((data['weight'][msk_fail]).shape))
        dtype = ['rhoDDT','logpt', 'ANN_score', 'pt', 'm', 'weight']
        dtype = [(var, 'f8') for var in dtype]
        toys_pass = np.zeros(N_pass, dtype=dtype)
        toys_fail = np.zeros(N_fail, dtype=dtype)

        print "Generating toys (pass: %d, fail: %d)" % (N_pass, N_fail)
        rds_pass = rhp_pass.generate(ROOT.RooArgSet(rhoDDT, logpt), N_pass, True, False)
        rds_fail = rhp_fail.generate(ROOT.RooArgSet(rhoDDT, logpt), N_fail, True, False)

        for idx in range(N_pass):
            toys_pass['rhoDDT']  [idx] = rds_pass.get(idx).getRealValue('rhoDDT')
            toys_pass['logpt']   [idx] = rds_pass.get(idx).getRealValue('logpt')
            toys_pass['pt']      [idx] = np.exp(toys_pass['logpt'][idx])
            #toys_pass['m']       [idx] = np.sqrt(np.exp(toys_pass['rhoDDT'][idx]) * toys_pass['pt'][idx] * 1.)
            toys_pass['m']       [idx] = np.sqrt(np.exp(toys_pass['rhoDDT'][idx])) * toys_pass['pt'][idx] * 1. 
            toys_pass['weight']  [idx] = 1. / float(mult)
            toys_pass['ANN_score'][idx] = 1.
            pass

        for idx in range(N_fail):
            toys_fail['rhoDDT']  [idx] = rds_fail.get(idx).getRealValue('rhoDDT')
            toys_fail['logpt']   [idx] = rds_fail.get(idx).getRealValue('logpt')
            toys_fail['pt']      [idx] = np.exp(toys_fail['logpt'][idx])
            #toys_fail['m']       [idx] = np.sqrt(np.exp(toys_fail['rhoDDT'][idx]) * toys_fail['pt'][idx] * 1.)
            toys_fail['m']       [idx] = np.sqrt(np.exp(toys_fail['rhoDDT'][idx])) * toys_fail['pt'][idx] * 1.
            toys_fail['weight']  [idx] = 1. / float(mult)
            toys_fail['ANN_score'][idx] = 0.
            pass

        data = np.concatenate((toys_pass, toys_fail)) # ???
        pass

    # Transfer factor
    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    calc = calculator(data=data, config=config) # Using default configuration
    #calc = calculator(data=bkg, config=config) # Using default configuration
    calc.mass = mass
    calc.window = window
    print(calc.window)
    #calc.fullfit(prefix=prefix)
    calc.fit(prefix=prefix)

    # calcSig = calculator(data=signal_nomu, config=config) # Using default configuration
    # calcSig.mass = mass
    # calcSig.fullfit(prefix=prefix)

    # Pass/fail masks
    msk_data_pass = config['pass'](data)
    msk_sig_pass  = config['pass'](signal)
    msk_bkg_pass = config['pass'](bkg)

    msk_data_fail = ~msk_data_pass
    msk_sig_fail  = ~msk_sig_pass
    msk_bkg_fail = ~msk_bkg_pass

    testNpass=int(np.sum(data['weight'][msk_data_pass]))
    print("testNpass")
    print(testNpass)

    print "  -- Computing data weights"
    #w_nom, w_up, w_down  = calc.fullweights(np.asarray(data[config["params"][0]])[msk_data_fail], np.asarray(data[config["params"][1]])[msk_data_fail])
    w_nom  = calc.weights(np.asarray(data[config["params"][0]])[msk_data_fail], np.asarray(data[config["params"][1]])[msk_data_fail])
    w_up   = calc.weights(np.asarray(data[config["params"][0]])[msk_data_fail], np.asarray(data[config["params"][1]])[msk_data_fail], shift=+1)
    w_down = calc.weights(np.asarray(data[config["params"][0]])[msk_data_fail], np.asarray(data[config["params"][1]])[msk_data_fail], shift=-1)

    print "  -- Computing signal weights"
    #w_sig, _, _ = calc.fullweights(np.asarray(signal[config["params"][0]])[msk_sig_fail], np.asarray(signal[config["params"][1]])[msk_sig_fail])
    #w_sig = calc.weights(np.asarray(signal[config["params"][0]])[msk_sig_fail], np.asarray(signal[config["params"][1]])[msk_sig_fail])

    print "  -- Final fit done"
    if save:
        calc.plot(save=save, prefix=prefix+'/signalinjection_%s%s_' % ("toys_" if args.toys else "", "injected" if args.inject else "notinjected"))
        #calcSig.plot(save=save, prefix=prefix+'/signalinjectionSig_%s%s_' % ("toys_" if args.toys else "", "injected" if args.inject else "notinjected"))


    # Performing signal injection test
    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    extMuNom    = []
    extMuNomErr = []
    extMuUp    = []
    extMuUpErr = []
    extMuDown    = []
    extMuDownErr = []

    injectedmu = []
    df = pd.DataFrame([0], columns=['Injected'])
    extractedmu = []
    errextractedmu = []
    if True or args.show or args.save:

        bestfit_mu = None

        for mu, fit, prefit, subtract in zip([SigStrength,     None],
                                             [True,  False],
                                             [True,  False],
                                             [False, True]):


            if prefit:
                injmu = mu

            if prefit == False:
                mu = bestfit_mu[0]
                print("Final result: [{}, {}]".format(mu, bestfit_mu[1]))
                pass


            # Plotting
            # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

            c = ap.canvas(num_pads=2, batch=True)
            p0, p1 = c.pads()

            # -- Histograms: Main pad
            bins = config['massbins']

            #print(data['m'][msk_data_fail])
            h_bkg      = c.hist(data['m'][msk_data_fail], bins=bins, weights=data['weight'][msk_data_fail] * w_nom,  display=False)
            h_bkg_up   = c.hist(data['m'][msk_data_fail], bins=bins, weights=data['weight'][msk_data_fail] * w_up,   display=False)
            h_bkg_down = c.hist(data['m'][msk_data_fail], bins=bins, weights=data['weight'][msk_data_fail] * w_down, display=False)

            # h_bkg      = c.hist(bkg['m'][msk_bkg_fail], bins=bins, weights=bkg['weight'][msk_bkg_fail] * w_nom,  display=False)
            # h_bkg_up   = c.hist(bkg['m'][msk_bkg_fail], bins=bins, weights=bkg['weight'][msk_bkg_fail] * w_up,   display=False)
            # h_bkg_down = c.hist(bkg['m'][msk_bkg_fail], bins=bins, weights=bkg['weight'][msk_bkg_fail] * w_down, display=False)

            # h_sig = c.hist(signal['m'][msk_sig_pass], bins=bins, weights=signal['weight'][msk_sig_pass],         scale=mu, display=False)
            # h_sfl = c.hist(signal['m'][msk_sig_fail], bins=bins, weights=signal['weight'][msk_sig_fail] * w_sig, scale=mu, display=False)

            if fit == False:
                h_sig = c.hist(signal['m'][msk_sig_pass], bins=bins, weights=signal['weight'][msk_sig_pass],         scale=mu, display=False)
                #h_sfl = c.hist(signal['m'][msk_sig_fail], bins=bins, weights=signal['weight'][msk_sig_fail] * w_sig, scale=mu, display=False)
                h_sfl = c.hist(signal['m'][msk_sig_fail], bins=bins, weights=signal['weight'][msk_sig_fail], scale=mu, display=False) 
            else:
                h_sig = c.hist(signal['m'][msk_sig_pass], bins=bins, weights=signal['weight'][msk_sig_pass],         display=False)
                #h_sfl = c.hist(signal['m'][msk_sig_fail], bins=bins, weights=signal['weight'][msk_sig_fail] * w_sig, display=False)
                h_sfl = c.hist(signal['m'][msk_sig_fail], bins=bins, weights=signal['weight'][msk_sig_fail], display=False)

            h_data = c.plot(data['m'][msk_data_pass], bins=bins, weights=data['weight'][msk_data_pass], display=False)

            # for bin in range(1, h_bkg.GetXaxis().GetNbins() + 1):
            #     width = float(h_bkg.GetBinWidth(bin))
            #     h_bkg     .SetBinContent(bin, h_bkg     .GetBinContent(bin) / width)
            #     h_bkg     .SetBinError  (bin, h_bkg     .GetBinError  (bin) / width)
            #     h_bkg_up  .SetBinContent(bin, h_bkg_up  .GetBinContent(bin) / width)
            #     h_bkg_up  .SetBinError  (bin, h_bkg_up  .GetBinError  (bin) / width)
            #     h_bkg_down.SetBinContent(bin, h_bkg_down.GetBinContent(bin) / width)
            #     h_bkg_down.SetBinError  (bin, h_bkg_down.GetBinError  (bin) / width)
            #     h_sig     .SetBinContent(bin, h_sig     .GetBinContent(bin) / width)
            #     h_sig     .SetBinError  (bin, h_sig     .GetBinError  (bin) / width)
            #     h_sfl     .SetBinContent(bin, h_sfl     .GetBinContent(bin) / width)
            #     h_sfl     .SetBinError  (bin, h_sfl     .GetBinError  (bin) / width)
            #     h_data    .SetBinContent(bin, h_data    .GetBinContent(bin) / width)
            #     h_data    .SetBinError  (bin, h_data    .GetBinError  (bin) / width)
            #     pass

            if fit == False: ## 2nd iteration, maybe change this?
               h_bkg     .Add(h_sfl, -1) # Subtracting signal
               h_bkg_up  .Add(h_sfl, -1) # --
               h_bkg_down.Add(h_sfl, -1) # --
               pass

            c.hist(h_bkg, option='HIST', linestyle=0, fillstyle=0, fillcolor=0)  # Staring with standard histogram, not THStack, just to get y-axis to cooperate
            h_bkg = c.stack(h_bkg,
                            fillcolor=ROOT.kAzure + 7,
                            label='Background pred.')
            h_sig = c.stack(h_sig,
                            fillcolor=ROOT.kRed - 4,
                            label="Z' (#mu = %s)" % ("%.0f" % mu if prefit else "%.2f #pm %.2f" % (mu, bestfit_mu[1])))

            h_sum = h_bkg
            h_sum = c.hist(h_sum,
                           fillstyle=3245, fillcolor=ROOT.kGray + 3, option='E2',
                           label='Stat. uncert.')

            h_bkg_up   = c.hist(h_bkg_up,
                                linecolor=ROOT.kGreen + 1, linestyle=2, option='HIST',
                                label='Syst. uncert.')
            h_bkg_down = c.hist(h_bkg_down,
                                linecolor=ROOT.kGreen + 1, linestyle=2, option='HIST')


            h_data = c.plot(h_data,
                            label='Pseudo-data')

            c.hist(h_bkg, option='AXIS')  # Re-draw axes

            # -- Histograms: Ratio pad
            c.ratio_plot((h_sig,      h_sum), option='HIST', offset=1)
            c.ratio_plot((h_sum,      h_sum), option='E2')
            c.ratio_plot((h_bkg_up,   h_sum), option='HIST')
            c.ratio_plot((h_bkg_down, h_sum), option='HIST')
            c.ratio_plot((h_data,     h_sum))

            # -- Text
            c.text(["#sqrt{s} = 13 TeV,  L = 36.1 fb^{-1}",
                    #"Sherpa incl. #gamma MC",
                    ("Trimmed anti-k_{t}^{R=1.0} jets" if args.jetcol=="fatjet_" else "UFO anti-k_{t}^{R=1.0} jets"),
                    #"Trimmed anti-k_{t}^{R=1.0} jets",
                    #"ISR #gamma selection",
                    "Window: %d GeV #pm %d %%" % (args.mass, window * 100.),
                    ("Signal" if args.inject else "No signal") + " injected",
                   ] + (["Using toys"] if args.toys else []), qualifier='Simulation Internal')

            # -- Axis labels
            c.xlabel('Large-#it{R} jet mass [GeV]')
            c.ylabel('Events / 5 GeV')
            p1.ylabel('Data / Est.')

            # -- Log
            c.log()

            # -- Axis limits
            #c.ylim(1.0E+00, 1.0E+06)
            p1.ylim(0.80, 1.20)

            # -- Line(s)
            p1.yline(1.0)

            # -- Region(s)
            # c.region("SR", 0.8 * args.mass, 1.2 * args.mass)
            #c.region("SR", (1-args.window-0.1) * args.mass, (1+args.window) * args.mass)
            c.region("SR", (1-args.window) * args.mass, (1+args.window) * args.mass)


            c.legend()
            if args.show and not fit: c.show()
            if args.save and not fit: c.save(prefix+'/signalinjection_%s%dGeV_pm%d_%s_%s.png' % (
                    "toys_" if args.toys else "",
                    args.mass,
                    20.,
                    #('prefit_mu%d' % mu if prefit else 'postfit'),
                    ('prefit_mu%d' % mu if prefit else 'postfit_mu%d' % injmu),
                    ('injected' if args.inject else 'notinjected')
                    ))



            # Fitting
            # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            if fit:
                print("Injected mu = {}".format(mu))
                injectedmu.append(mu)
                bestfit_mu = list()

                hs_save = [
                    h_bkg_down.Clone('h_save_down'),
                    h_bkg     .Clone('h_save_nom'),
                    h_bkg_up  .Clone('h_save_up'),
                    ]

                for variation in range(3):

                    print "Variation: " + ("Nominal" if variation == 1 else ("Up" if variation == 2 else "Down"))

                    # Get correct histogram fore this variation
                    h_bkg_use = hs_save[variation]

                    # Debug fitting
                    debug = False
                    if debug==True:
                        print("{}  {}".format(variation,h_bkg_use.GetName() ))


                    # -- Define jet mass variable
                    mJ = ROOT.RooRealVar('mJ', 'mJ', 50, 300)
                    #mJ.setBins(50)
                    roobinning = ROOT.RooBinning(len(config['massbins']) - 1, config['massbins'])
                    mJ.setBinning(roobinning)

                    # -- Define histograms
                    rdh_bkg = ROOT.RooDataHist('rdh_bkg', 'rdh_bkg', ROOT.RooArgList(mJ), h_bkg_use)
                    rdh_sig = ROOT.RooDataHist('rdh_sig', 'rdh_sig', ROOT.RooArgList(mJ), h_sig)
                    rdh_sfl = ROOT.RooDataHist('rdh_sfl', 'rdh_sfl', ROOT.RooArgList(mJ), h_sfl)

                    # -- Turn histograms into pdf's
                    rhp_bkg = ROOT.RooHistPdf('rhp_bkg', 'rhp_bkg', ROOT.RooArgSet(mJ), rdh_bkg)
                    rhp_sig = ROOT.RooHistPdf('rhp_sig', 'rhp_sig', ROOT.RooArgSet(mJ), rdh_sig)
                    rhp_sfl = ROOT.RooHistPdf('rhp_sfl', 'rhp_sfl', ROOT.RooArgSet(mJ), rdh_sfl)

                    # -- Define integrals as constants
                    n_bkg = ROOT.RooRealVar('n_bkg', 'n_bkg', h_bkg_use.Integral())
                    n_sig = ROOT.RooRealVar('n_sig', 'n_sig', h_sig.Integral())
                    n_sfl = ROOT.RooRealVar('n_sfl', 'n_sfl', h_sfl.Integral())

                    v_mu   = ROOT.RooRealVar('v_mu',   'v_mu', SigStrength, -10, 10) ## 1 0 5
                    v_neg1 = ROOT.RooRealVar('v_neg1', 'v_neg1', -1)

                    # -- Define fittable normalisation factors
                    c_bkg = ROOT.RooFormulaVar('c_bkg', 'c_bkg', '@0',           ROOT.RooArgList(n_bkg))
                    c_sig = ROOT.RooFormulaVar('c_sig', 'c_sig', '@0 * @1',      ROOT.RooArgList(v_mu, n_sig))
                    c_sfl = ROOT.RooFormulaVar('c_sfl', 'c_sfl', '@0 * @1 * @2', ROOT.RooArgList(v_neg1, v_mu, n_sfl))

                    # -- Construct combined pdf
                    pdf = ROOT.RooAddPdf('pdf', 'pdf', ROOT.RooArgList(rhp_bkg, rhp_sig, rhp_sfl), ROOT.RooArgList(c_bkg, c_sig, c_sfl)) ## 2nd iteration, maybe change this
                    #pdf = ROOT.RooAddPdf('pdf', 'pdf', ROOT.RooArgList(rhp_bkg, rhp_sig), ROOT.RooArgList(c_bkg, c_sig))
                    # -- Construct data histogram
                    rdh_data = ROOT.RooDataHist('rdh_data', 'rdh_data', ROOT.RooArgList(mJ), h_data)

                    # -- Fit pdf to data histogram
                    # -- Method 1
                    arglist = ROOT.RooLinkedList()
                    saveArg = ROOT.RooFit.Save()
                    arglist.Add(saveArg)
                    pdf1 = pdf.chi2FitTo(rdh_data, arglist)
                    pdf1.Print()
                    # -- Method 2
                    # chi2Fit = ROOT.RooChi2Var("chi2","chi2", pdf, rdh_data, ROOT.RooFit.Extended())
                    # minuit = ROOT.RooMinuit (chi2Fit)
                    # minuit.migrad()
                    # minuit.hesse()
                    # r2 = minuit.save()
                    # r2.Print("v")
                    # print("here:  {}".format(r2.minNll()))

                    print "Best fit mu: %.3f +/- %.3f" % (v_mu.getValV(), v_mu.getError())
                    bestfit_mu.append( (v_mu.getValV(), v_mu.getError()) )

                    if debug:
                        cJMShistos   = ROOT.TCanvas("", "", 500, 500)
                        framehisto = mJ.frame(ROOT.RooFit.Name("xframe"), ROOT.RooFit.Title("Red Curve / SumW2 Histo errors"), ROOT.RooFit.Bins(50))
                        rdh_bkg.plotOn(framehisto, ROOT.RooFit.LineColor(ROOT.kViolet + 7), ROOT.RooFit.MarkerColor(ROOT.kViolet + 7))
                        rdh_sig.plotOn(framehisto, ROOT.RooFit.LineColor(ROOT.kTeal), ROOT.RooFit.MarkerColor(ROOT.kTeal))
                        rdh_sfl.plotOn(framehisto, ROOT.RooFit.LineColor(ROOT.kOrange - 3), ROOT.RooFit.MarkerColor(ROOT.kOrange - 3))
                        framehisto.Draw()
                        cJMShistos.SetLogy()
                        cJMShistos.SaveAs(prefix+"/histos_mu"+str(mu)+"_var"+str(variation)+".png")

                    if debug:
                        cJMSpdfs     = ROOT.TCanvas("", "", 500, 500)
                        framepdf = mJ.frame(ROOT.RooFit.Name("xframe"), ROOT.RooFit.Title("Red Curve / SumW2 Histo errors"), ROOT.RooFit.Bins(50))
                        rhp_bkg.plotOn(framepdf, ROOT.RooFit.LineColor(ROOT.kViolet + 7), ROOT.RooFit.MarkerColor(ROOT.kViolet + 7))
                        rhp_sig.plotOn(framepdf, ROOT.RooFit.LineColor(ROOT.kTeal),       ROOT.RooFit.MarkerColor(ROOT.kTeal))
                        rhp_sfl.plotOn(framepdf, ROOT.RooFit.LineColor(ROOT.kOrange - 3), ROOT.RooFit.MarkerColor(ROOT.kOrange - 3))
                        framepdf.Draw()
                        cJMSpdfs.SaveAs(prefix+"/pdfs_mu"+str(mu)+"_var"+str(variation)+".png")

                    if debug:
                        cJMScombpdf  = ROOT.TCanvas("", "", 500, 500)
                        frame = mJ.frame(ROOT.RooFit.Name("xframe"), ROOT.RooFit.Title("Red Curve / SumW2 Histo errors"), ROOT.RooFit.Bins(50))
                        pdf.plotOn(frame)
                        frame.Draw()
                        cJMScombpdf.SaveAs(prefix+"/combpdf_mu"+str(mu)+"_var"+str(variation)+".png")

                    if debug:
                        cJMSdatahist = ROOT.TCanvas("", "", 500, 500)
                        frame1 = mJ.frame(ROOT.RooFit.Name("xframe"), ROOT.RooFit.Title("Red Curve / SumW2 Histo errors"), ROOT.RooFit.Bins(50))
                        rdh_data.plotOn(frame1)
                        frame1.Draw()
                        cJMSdatahist.SaveAs(prefix+"/datahist_mu"+str(mu)+"_var"+str(variation)+".png")

                    if debug:
                        cJMSpdffit   = ROOT.TCanvas("", "", 500, 500)
                        frame2 = mJ.frame(ROOT.RooFit.Name("xframe"), ROOT.RooFit.Title("Red Curve / SumW2 Histo errors"), ROOT.RooFit.Bins(50))
                        rdh_data.plotOn(frame2, ROOT.RooFit.LineColor(ROOT.kViolet + 7), ROOT.RooFit.MarkerColor(ROOT.kViolet + 7))
                        pdf.plotOn(frame2, ROOT.RooFit.LineColor(ROOT.kPink+3), ROOT.RooFit.MarkerColor(ROOT.kPink+3))
                        rdh_bkg.plotOn(frame2, ROOT.RooFit.LineColor(ROOT.kTeal),       ROOT.RooFit.MarkerColor(ROOT.kTeal))
                        rdh_sig.plotOn(frame2, ROOT.RooFit.LineColor(ROOT.kSpring - 2), ROOT.RooFit.MarkerColor(ROOT.kSpring - 2))
                        rdh_sfl.plotOn(frame2, ROOT.RooFit.LineColor(ROOT.kOrange - 3), ROOT.RooFit.MarkerColor(ROOT.kOrange - 3))
                        frame2.Draw()
                        cJMSpdffit.SetLogy()
                        cJMSpdffit.SaveAs(prefix+"/pdffit_mu"+str(mu)+"_var"+str(variation)+".png")
                    if variation==0:
                        extMuDown.append(v_mu.getValV())
                        extMuDownErr.append(v_mu.getError())
                    if variation==1:
                        extMuNom.append(v_mu.getValV())
                        extMuNomErr.append(v_mu.getError())
                    if variation==2:
                        extMuUp.append(v_mu.getValV())
                        extMuUpErr.append(v_mu.getError())
                    pass
                print(bestfit_mu)
                bestfit_mu = bestfit_mu[1][0], np.sqrt(np.power(abs(bestfit_mu[0][0] - bestfit_mu[2][0]) / 2., 2.) + np.power(bestfit_mu[1][1], 2.))
                df["Extracted_Nominal"]     = extMuNom
                df["Extracted_Nominal_Err"] = extMuNomErr
                df["Extracted_Up"]          = extMuUp
                df["Extracted_Up_Err"]      = extMuUpErr
                df["Extracted_Down"]        = extMuDown
                df["Extracted_Down_Err"]    = extMuDownErr
                df.to_csv(prefix+"/result_spurious_signal.csv", sep=" ", index=False)
                pass

            pass

        pass

    return



# Main function call.
if __name__ == '__main__':
    main()
    pass
