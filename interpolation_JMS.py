#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Script for ...

@file:   interpolation.py
@author: Andreas Søgaard
@date:   14 June 2017
@email:  andreas.sogaard@cern.ch
"""

# Basic import(s)
import sys, glob

# Get ROOT to stop hogging the command-line options
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
from calculator_JMS import calculator
import functionsflattener
import pandas as pd

# Scientific import(s)
try:
    # Numpy
    import numpy as np
    from root_numpy import *

    import matplotlib.pyplot as plt
except ImportError:
    print "WARNING: One or more scientific python packages were not found. If you're in lxplus, try running:"
    print "         $ source /cvmfs/sft.cern.ch/lcg/views/LCG_88/x86_64-slc6-gcc49-opt/setup.sh"
    sys.exit()
    pass

# Local import(s)
try:
    #import transferfactor as tf
    #from transferfactor.utils import make_directories, check_make_dir
    from rootplotting import ap
    from rootplotting.tools import *
except ImportError:
    print "WARNING: This script uses the 'transferfactor' and 'rootplotting' packages. Clone them as e.g.:"
    print "         $ git clone git@github.com:asogaard/transferfactor.git"
    print "         $ git clone git@github.com:asogaard/rootplotting.git"
    sys.exit()
    pass


# Command-line arguments parser
import argparse

parser = argparse.ArgumentParser(description='Perform signal mass spectrum interpolation.')

parser.add_argument('--show', dest='show', action='store_const',
                    const=True, default=False,
                    help='Show plots (default: False)')
parser.add_argument('--save', dest='save', action='store_const',
                    const=True, default=False,
                    help='Save plots (default: False)')
parser.add_argument('--rhoDDTminCut', dest='rhoDDTminCut', type=float,
                    default=1.5,
                    help='Lower cut on rhoDDT (default: 1.5)')
parser.add_argument('--rhoDDTmaxCut', dest='rhoDDTmaxCut', type=float,
                    default=999,
                    help='Higher cut on rhoDDT (default: 999)')
parser.add_argument('--ptCut', dest='ptCut', type=float,
                    default=450,
                    help='Cut on fatjet pT (default: 450)')
parser.add_argument('--jetcol', dest='jetcol', type=str,
                    default='',
                    help='fatjet_ or UFOjet_')
parser.add_argument('--outdir', dest='outdir', type=str,
                    default='',
                    help='Output directory')
args = parser.parse_args()

if args.jetcol=="fatjet_":
    from config import config
if args.jetcol=="UFOjet_":
    from config_UFO import config


# Main function.
def main ():

    massPoints = np.array([60, 80, 100, 150, 175, 200, 225])
    colours = [ROOT.kViolet + 7, ROOT.kAzure + 7, ROOT.kTeal, ROOT.kSpring - 2, ROOT.kOrange - 3, ROOT.kPink,  ROOT.kPink+3]

    # Setup.
    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    #files  = sorted(glob.glob(config['base_path'] + config['bkg_dir'] + '/Sig_MC16a_mass*.root'), key=len)
    files = glob.glob(config['base_path'] + config['bkg_dir'] + '/Sig_MC16a_mass{}.root'.format(massPoints[0]))
    for imass in range(1,len(massPoints)):
        files  += glob.glob(config['base_path'] + config['bkg_dir'] + '/Sig_MC16a_mass{}.root'.format(massPoints[imass]))


    if len(files) == 0:
        warning("No files found.")
        return

    interpolation_loadData(files, config['treeparams'], massPoints, colours, save=args.save, ptCut=args.ptCut,
                            rhoDDTminCut=args.rhoDDTminCut,  rhoDDTmaxCut=args.rhoDDTmaxCut, jetcol=args.jetcol, prefix=args.outdir)

def interpolation_loadData (files, branches, massPoints, colours, save, ptCut, rhoDDTminCut, rhoDDTmaxCut, jetcol, prefix = '', fast = True):


    massVec = ROOT.TVectorD(len(massPoints))
    integrals = np.zeros((len(massPoints),), dtype=float)
    events    = np.zeros((len(massPoints),), dtype=float)
    for i in range(len(massPoints)):
        massVec[i] = massPoints[i]
        pass
    workspace = ROOT.RooWorkspace()
    mZ = workspace.factory('mZ[50,300]') # this is our continuous interpolation parameter
    mJ = workspace.factory('mJ[50,300]')
    #mZ = workspace.factory('mZ[50,200]') # this is our continuous interpolation parameter
    #mJ = workspace.factory('mJ[50,200]')
    mJ.setBins(50)
    #mJ.setBins(35)
    frame = mJ.frame()

    for i in range(len(massPoints)):
        workspace.factory('Gaussian::signal{i}(mJ, mu_sig_{i}[{mean},0,300], sigma_sig_{i}[{width},0,50])'.format(i=i, mean=massPoints[i], width=massPoints[i]*0.1))
        #workspace.factory('Gaussian::background{i}(mJ, mu_bkg_{i}[0,0,0], sigma_bkg_{i}[100,50,500])'.format(i=i))
        workspace.factory('Exponential::background{i}(mJ, tau_bkg_{i}[-0.01,-100,0])'.format(i=i))

        workspace.factory('SUM::model{i}(norm_sig_{i}[2,0,10]*signal{i}, norm_bkg_{i}[1,1,1]*background{i})'.format(i=i))
        pass


    c = ap.canvas(batch=True)
    bins = np.linspace(50, 300, 50 + 1, endpoint=True)
    #bins = np.linspace(50, 200, 35 + 1, endpoint=True)

    for i in range(len(massPoints)):
        print(i)
    pdfs = ROOT.RooArgList(*[workspace.pdf('model{i}'.format(i=i)) for i in range(len(massPoints))])
    print(pdfs)
    key_to_del = ['ANN_score', 'tau21_wta']
    count = 0
    for cfile in files:
        data1 = functionsflattener.getDataDictJMS(cfile, branches, config, jetcol, doNorm=True)
        if not data1:
            continue



        for key in key_to_del:
            del data1[key]
        dataframe = pd.DataFrame.from_dict(data1)
        dataframe = dataframe[(dataframe['pt'] > ptCut) & (dataframe['rhoDDT'] > rhoDDTminCut) & (dataframe['rhoDDT'] < rhoDDTmaxCut) &  (dataframe['pt'] / dataframe['m'] > 2)]
        print(len(dataframe.index))
        hist = c.hist(np.array(dataframe.m), bins=bins, weights=np.array(dataframe.weight), normalise=True)
        integrals[count] = np.sum(np.array(dataframe.weight))
        events   [count] = len(dataframe.index)

        # Add minimal error to empty bins
        emax = np.finfo(float).max
        for bin in range(1, hist.GetXaxis().GetNbins() + 1):
            if hist.GetBinError(bin) == 0: hist.SetBinError(bin, emax)
            pass

        rdh = ROOT.RooDataHist('rdh', 'rdh', ROOT.RooArgList(mJ), hist)
        rhp = ROOT.RooHistPdf ('rhp', 'rhp', ROOT.RooArgSet (mJ), rdh)

        # Fit data with model
        pdfs[count].chi2FitTo(rdh, ROOT.RooLinkedList())

        # Plot data histogram and fit
        rhp      .plotOn(frame, ROOT.RooFit.LineColor(colours[count]), ROOT.RooFit.LineStyle(1), ROOT.RooFit.LineWidth(3))
        pdfs[count].plotOn(frame, ROOT.RooFit.LineColor(ROOT.kBlack), ROOT.RooFit.LineStyle(2))
        count+=1

    setting = ROOT.RooMomentMorph.Linear
    morph = ROOT.RooMomentMorph('morph', 'morph', mZ, ROOT.RooArgList(mJ), pdfs, massVec, setting)
    getattr(workspace,'import')(morph) # work around for morph = w.import(morph)
    morph.Print('v')

    #interpolationMassPoints = np.linspace(100, 175, (175 - 100) / 5 + 1, endpoint=True)
    #interpolationMassPoints = np.array([110, 160, 176])
    interpolationMassPoints = np.array([65, 85, 105, 155, 180, 205])
    #interpolationMassPoints = np.array([60, 70, 80, 100, 150, 170, 200])
    #interpolationMassPoints = np.linspace(50, 200, (200 - 50) / 5 + 1, endpoint=True)
    interpolationMassPoints = sorted(list(set(interpolationMassPoints) - set(massPoints)))

    interpolationIntegrals = np.exp(np.interp(interpolationMassPoints, massPoints, np.log(integrals)))
    interpolationEvents    =        np.interp(interpolationMassPoints, massPoints, events).astype(int)

    for i, mass in enumerate(interpolationMassPoints):
        print "=" * 80
        mZ.setVal(mass)
        mZ.Print()
        morph.Print()
        morph.plotOn(frame, ROOT.RooFit.LineColor(ROOT.kRed - 4), ROOT.RooFit.LineStyle(2), ROOT.RooFit.LineWidth(1))
        pass

    c = ap.canvas(batch=True)
    c.pads()[0]._bare().cd()
    frame.Draw()
    frame.GetXaxis().SetTitle("Large-#it{R} jet mass [GeV]")
    frame.GetYaxis().SetTitle("Signal p.d.f.")
    frame.GetYaxis().SetRangeUser(0, 0.22)
    c.text(["#sqrt{s} = 13 TeV"], qualifier="Simulation Internal")
    c.legend(header="Signal MC shape:",
                 categories= [ ("Z' (%d GeV)" % mass, {'linecolor': colours[idx], 'linewidth': 3, 'option': 'L'}) for idx, mass in enumerate(massPoints)]
                + [
                ("Fitted  shape",      {'linecolor': ROOT.kBlack,   'linestyle': 2, 'linewidth': 2, 'option': 'L'}),
                ("Interpolated shape", {'linecolor': ROOT.kRed - 4, 'linestyle': 2, 'linewidth': 1, 'option': 'L'}),
                ])
    c.ylim(0, 0.3)
    if save: c.save(prefix+"/distribution_multiplemasses.png")

    return


# Main function call.
if __name__ == '__main__':
    main()
    pass
